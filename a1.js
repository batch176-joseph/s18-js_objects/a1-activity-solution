function Pokemon (name, level) {
	//properties
	this.name = name;
	this.level = level;
	this.health = 50 * level
	this.attack = level * 8

	//methods
	this.tackle = function(target) {
		console.log(this.name + ` tackled ` + target.name);
		console.log(target.name + ` received ` + this.attack + ` damage`);
		target.health = target.health - this.attack;
		console.log(target.name + `'s health is at ` + target.health);
		
		if (target.health <= 5){
				target.faint();
			}
	};
	this.faint = function(target){
		console.warn(this.name + `'s health has been depleted`)
		console.warn(this.name + ` fainted.`);
	}

}

// 1.) Create a new set of pokemon for pokemon battle. (Same as our discussion.)

let dialga = new Pokemon(`Dialga`, 25)
//1250 HP 200 ATK

let regigigas = new Pokemon(`Regigigas`, 13)
//650 HP 104 ATK

let moltres = new Pokemon(`Moltres`, 21)
//1050 HP 168 ATK

let articuno = new Pokemon(`Articuno`, 17)
//850 HP 136 ATK

let zapdos = new Pokemon(`Zapdos`, 23)
//1150 HP 184 ATK

// 2.) If health is below 5, invoke faint function.
console.log(`Choose your pokemon and target`)
console.log(dialga);
console.log(regigigas);
console.log(moltres);
console.log(articuno);
console.log(zapdos);
console.log();

/*
dialga.tackle(regigigas);
dialga.tackle(moltres);
dialga.tackle(articuno);
dialga.tackle(zapdos);

regigigas.tackle(dialga);
regigigas.tackle(moltres);
regigigas.tackle(articuno);
regigigas.tackle(zapdos);

moltres.tackle(dialga);
moltres.tackle(regigigas);
moltres.tackle(articuno);
moltres.tackle(zapdos);

articuno.tackle(dialga);
articuno.tackle(regigigas);
articuno.tackle(moltres);
articuno.tackle(zapdos);

zapdos.tackle(dialga);
zapdos.tackle(regigigas);
zapdos.tackle(moltres);
zapdos.tackle(articuno);
*/